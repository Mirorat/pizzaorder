import com.mirorat.model.pizza.PizzaFactory;
import org.mockito.Mock;
import service.CheeseService;
import service.CurrentPizzaService;


public class MockedPizzaServiceTests {

    @Mock
    private CheeseService cheeseService;

    @Mock
    private PizzaFactory pizzaFactory;

    private CurrentPizzaService service = new CurrentPizzaService(pizzaFactory, cheeseService);

//    @BeforeMethod
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//    } -- disabled as currentPizzaService creation changed

//    @Test
//    public void should_ReturnRegularMilk_If_GetCheeseCalled()  {
//        // given
//        String cheeseName = "test";
//        given(cheeseService.getCheeseFromMilk(cheeseName)).willReturn("regular milk");
//
//        // when
//        String cheese = service.getCheese(cheeseName);
//
//        //then
//        Assert.assertEquals(cheese, "regular milk");
////        Mockito.anyString();
////        Mockito.eq()
//
//    }
}
