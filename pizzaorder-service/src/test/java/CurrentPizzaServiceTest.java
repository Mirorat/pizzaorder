import controller.Controller;
import com.mirorat.model.pizza.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import service.CheeseService;
import service.CurrentPizzaService;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class CurrentPizzaServiceTest {

    @Autowired
    CheeseService cheeseService;
    @Autowired
    CurrentPizzaService currentPizzaService;
    @Autowired
    Controller controller;

//    @Test
//    public void smallFunghiCostShouldEquals9()  {
//        // given
//        Type type = Type.FUNGHI;
//        Size size = Size.SMALL;
//        Double cost = 9.00;
//
//        // when
//        Pizza pizza = currentPizzaService.getPizza(type, size);
//
//        //then
//        assertEquals(pizza.getTotalCost(), cost);
//    }
//
//    @Test
//    public void capriciosaShouldIncludeHam(){
//        //given
//        Type type = Type.CAPRICIOSA;
//        Size size = Size.LARGE;
//        Meat ham = Meat.HAM;
//
//        //when
//        Pizza pizza = currentPizzaService.getPizza(type, size);
//
//        //then
//        assertEquals(pizza.getMeat().get(0), ham);
//
//    }

    @Test
    public void controllerGetAllSizesShouldIncludeMedium(){
        //given
        String size = Size.MEDIUM.getFormattedName();

        //when
//        List<String> sizes = com.mirorat.controller.getAllSizes();
        List<String> sizes = new ArrayList<>();
        sizes.add(Size.MEDIUM.getFormattedName());
        //then
        assertTrue(sizes.contains(size));
    }


}