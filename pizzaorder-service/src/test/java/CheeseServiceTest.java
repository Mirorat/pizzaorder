import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import service.CheeseService;
import service.MilkService;

public class CheeseServiceTest {

    @Mock
    MilkService milkService;

    CheeseService service = new CheeseService(milkService);

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

//    @Test
//    public void should_GetCheeseFromFreshMilk_If_MilkPassed() {
//        //given
//        Milk milk = new Milk(); // if milk is 'big' object it would be better to mock it
//        milk.setMilkType("Delicious Milk");
//        given(milkService.refreshMilk()).willReturn(milk);
//        String excpected = "Gorgonzola from Delicious Milk";
//        //when
//        String actual = service.getCheeseFromFreshMilk("Gorgonzola");
//
//        //then
//        Assert.assertEquals(actual, excpected);
//    }
}