package controller;

import com.mirorat.model.PizzaOrder;
import com.mirorat.model.PaymentMethod;
import com.mirorat.model.pizza.Pizza;
import com.mirorat.model.pizza.Size;
import com.mirorat.model.pizza.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import service.CurrentPizzaServiceQualifier;
import service.OrderService;
import service.PizzaService;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class Controller {

    private static final Logger LOG = LoggerFactory.getLogger(Controller.class);

    @Autowired
    PizzaOrder currentPizzaOrder;

    @Autowired
    @Qualifier(CurrentPizzaServiceQualifier.NAME)
    private PizzaService pizzaService;

    private OrderService currentOrderService;

    private List<PizzaOrder> allPizzaOrders;


    @Autowired
    public Controller(OrderService currentOrderService) {
        this.currentOrderService = currentOrderService;
        this.allPizzaOrders = currentOrderService.getAllOrders();
    }

    public Pizza createPizza(String name, String size) {
        LOG.debug("Creating pizza using: {}", pizzaService.toString());
        Size pizzaSize = decodeSizeStringToSize(size);
        Type type = decodePizzaStringToType(name);
        LOG.debug("Creating pizza - setting size: {}", pizzaSize.toString());
        LOG.debug("Creating pizza - setting type: {}", type.toString());
        return pizzaService.getPizza(type, pizzaSize);
    }

    private Size decodeSizeStringToSize(String size) {
        return Stream
                .of(Size.values())
                .filter(v -> v.name().equals(size.toUpperCase()))
                .findFirst()
                .orElseThrow();
    }

    public void addPizzaToCurrentOrder(Pizza pizza) {
        pizzaService.addCheeseToPizza(true);
        pizzaService.getCheese("Gorgonzola");
        pizzaService.addCheeseToPizza(false);
        pizzaService.getCheese("Gorgonzola");

        String orderedPizzas = currentPizzaOrder.getOrderedPizzas();
        if(orderedPizzas.isEmpty()){
            orderedPizzas += pizza.toString();
        } else {
            orderedPizzas += ", " + pizza.toString();
        }
        this.currentPizzaOrder.setOrderedPizzas(orderedPizzas);
        if(LOG.isDebugEnabled()) {
            LOG.debug("Pizza added to current order: {}", pizza.toString());
        }
        currentPizzaOrder.setOrderCost(this.currentPizzaOrder.getOrderCost() + pizza.getTotalCost());
        LOG.debug("order costs recalculated");
    }

    public void removePizzaFromCurrentOrder(Pizza pizza) {
        String orderedPizzas = currentPizzaOrder.getOrderedPizzas();
//        orderedPizzas.remove(pizza);
        this.currentPizzaOrder.setOrderedPizzas(orderedPizzas);
        LOG.debug("Pizza removed from current order: {}", pizza.toString());
        currentPizzaOrder.setOrderCost(this.getCurrentOrderCost() - pizza.getTotalCost());
        LOG.debug("order costs recalculated");
    }


    public void setCurrentOrderComment(String comment) {
        if (comment.isEmpty()) {
            comment = "No Comment";
        }
        LOG.debug("Comment for current order set to: {}", comment);
        currentPizzaOrder.setComment(comment);
    }


    public void setCurrentOrderPaymentMethod(PaymentMethod paymentMethod) {
        currentPizzaOrder.setPaymentMethod(paymentMethod);
    }


    public void confirmCurrentOrder() {
        currentOrderService.addOrder(this.currentPizzaOrder);
        this.currentPizzaOrder = new PizzaOrder(); //is it possible to 'clear' Order using Spring?
    }


    public List<String> getOrders() {
        return this
                .allPizzaOrders
                .stream()
                .map(PizzaOrder::toString)
                .collect(toList());
    }


    public Double getCurrentOrderCost() {
        return currentPizzaOrder.getOrderCost();
    }


    public List<String> getAllSizes() {
        return Stream.of(Size.values()).map(Size::getFormattedName).collect(toList());
    }


    public List<String> getAllPaymentMethods() {
        return Stream
                .of(PaymentMethod.values())
                .map(PaymentMethod::getFormattedName)
                .collect(toList());
    }

    private Type decodePizzaStringToType(String pizzaName){
        return Stream
                .of(Type.values())
                .filter(v -> v.name().equals(pizzaName.toUpperCase()))
                .findFirst()
                .orElseThrow();
    }


    public String getCurrentOrderPizzas() {
        return currentPizzaOrder.getOrderedPizzas();
    }
}
