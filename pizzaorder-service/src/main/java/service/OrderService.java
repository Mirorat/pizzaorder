package service;

import com.mirorat.model.PizzaOrder;

import java.util.List;

public interface OrderService {

    void addOrder(PizzaOrder pizzaOrder);
    List<PizzaOrder> getAllOrders();
}
