package service;

import com.mirorat.model.pizza.Pizza;
import com.mirorat.model.pizza.Size;
import com.mirorat.model.pizza.Type;

public interface PizzaService {

    Pizza getPizza(Type type, Size size);
    void addCheeseToPizza(boolean isFresh);
    String getCheese(String cheeseType);

}
