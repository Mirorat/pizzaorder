package service;

import com.mirorat.model.pizza.Pizza;
import com.mirorat.model.pizza.PizzaFactory;
import com.mirorat.model.pizza.Size;
import com.mirorat.model.pizza.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("Current")
public class CurrentPizzaService implements PizzaService{


   @Autowired
    private PizzaFactory pizzaFactory;
    private CheeseService cheeseService;

    //JSR330
//    @Inject, @Named
//    public service.CurrentPizzaService(){};


    public CurrentPizzaService(PizzaFactory ps) {
    this.pizzaFactory = ps;
    }

    @Autowired
    public CurrentPizzaService(PizzaFactory pizzaFactory, CheeseService cheeseService) {
        this.pizzaFactory = pizzaFactory;
        this.cheeseService = cheeseService;
    }

    public Pizza getPizza(Type type, Size size) {
        return pizzaFactory.createPizza(type, size);
    }

    public void addCheeseToPizza(boolean fresh) {
        if (fresh) {
            System.out.println(cheeseService.getCheeseFromFreshMilk("Gorgonzola"));
        } else {
            System.out.println(cheeseService.getCheeseFromMilk("Gouda"));
        }
    }
    public String getCheese(String cheeseType){
        System.out.println(cheeseService.getCheeseFromMilk(cheeseType));
        return cheeseService.getCheeseFromMilk(cheeseType);
    }
}



