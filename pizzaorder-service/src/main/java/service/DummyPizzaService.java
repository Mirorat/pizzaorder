package service;

import com.mirorat.model.pizza.Pizza;
import com.mirorat.model.pizza.Size;
import com.mirorat.model.pizza.Type;
import org.springframework.stereotype.Service;

@Service("Dummy")
public class DummyPizzaService implements PizzaService{
    @Override
    public Pizza getPizza(Type type, Size size) {
        return null;
    }

    @Override
    public void addCheeseToPizza(boolean isFresh) {

    }

    @Override
    public String getCheese(String cheeseType) {
        return "";
    }
}
