package service;

public class CurrentPizzaServiceQualifier {

    public static final String NAME = "Current";

    private CurrentPizzaServiceQualifier() throws IllegalAccessException {
        throw new IllegalAccessException("This should never be called");
    }
}
