package service;

import org.springframework.stereotype.Component;

@Component
public class CheeseService {
    private MilkService milkService;


    public CheeseService(MilkService milkService) {
        this.milkService = milkService;
    }

    public String getCheeseFromFreshMilk(String cheeseName){
        return cheeseName + " from " + milkService.refreshMilk().getMilkType();

    }

    public String getCheeseFromMilk(String cheeseName){
        return cheeseName + " from " + milkService.milk.getMilkType();
    }
}
