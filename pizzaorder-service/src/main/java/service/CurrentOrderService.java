package service;

import com.google.common.collect.Lists;
import com.mirorat.model.PizzaOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.OrderRepository;
import repository.OrderRepositoryCustomImpl;

import java.util.List;

@Service
public class CurrentOrderService implements OrderService {

    OrderRepository repository;
    private static final Logger LOG = LoggerFactory.getLogger("src.main.java.service");


    @Autowired
    public CurrentOrderService(OrderRepository orderRepository) {
        this.repository = orderRepository;
    }

    public void addOrder(PizzaOrder pizzaOrder){
        repository.save(pizzaOrder);
        LOG.debug("order added to repository: {}", pizzaOrder);
        System.out.println("QUERY TEST");
        System.out.println(repository.findPizzaOrderByOrderCostGreaterThan(22.0));
        System.out.println(repository.findOrdersWithCostGreaterThanCustom(22.0));
        System.out.println(new OrderRepositoryCustomImpl().findOrdersWithCostGreaterThan(22.0));
        System.out.println(new OrderRepositoryCustomImpl().findOrdersWithCostGreaterThanHQL(22.0));
    }

    public List<PizzaOrder> getAllOrders(){
        return Lists.newArrayList(repository.findAll());
    }


}
