package service;

import com.mirorat.model.ingredient.Milk;
import org.springframework.stereotype.Component;

@Component
public class MilkService {

    public Milk milk;

    public MilkService(Milk milk) {
        this.milk = milk;
    }

    public Milk refreshMilk(){
        String milkType = this.milk.getMilkType();
        this.milk.setMilkType("Fresh" + milkType);
        return this.milk;
    }
}
