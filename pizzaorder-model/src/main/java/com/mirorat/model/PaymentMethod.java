package com.mirorat.model;


import org.apache.commons.text.WordUtils;

public enum PaymentMethod {
    CARD, CASH;

    public String getFormattedName() {
        return WordUtils.capitalizeFully(this.name());
    }
}


