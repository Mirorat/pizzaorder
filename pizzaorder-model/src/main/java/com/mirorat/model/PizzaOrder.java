package com.mirorat.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;

@Component
@Entity(name = "pizza_order")
public class PizzaOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name = "ordered_pizzas")
    private String orderedPizzas;

    @Column(name = "order_cost")
    private Double orderCost;

    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;

    private String comment;

    @Transient
    private String dummyTransientField;


    public PizzaOrder() {
        //empty constructor for hibernate
    }

    public PizzaOrder(BigInteger id, String orderedPizzas, Double orderCost, PaymentMethod paymentMethod, String comment) {
        this.id = id;
        this.orderedPizzas = orderedPizzas;
        this.orderCost = orderCost;
        this.paymentMethod = paymentMethod;
        this.comment = comment;
    }

    public void calculateOrderCost(double orderCost){
        this.orderCost = orderCost;
    }

    public String getOrderedPizzas() {
        return orderedPizzas;
    }

    public void setOrderedPizzas(String orderedPizzas) {
        this.orderedPizzas = orderedPizzas;
    }

    public Double getOrderCost() {
        return orderCost;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public void setOrderCost(Double orderCost) {this.orderCost = orderCost;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(id)
                .append(", ")
                .append(orderedPizzas)
                .append(", ")
                .append(orderCost)
                .append(", ")
                .append(paymentMethod.getFormattedName())
                .append(", ")
                .append(comment);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PizzaOrder pizzaOrder = (PizzaOrder) o;
        return id == pizzaOrder.id && Objects.equals(orderedPizzas, pizzaOrder.orderedPizzas) && Objects.equals(orderCost, pizzaOrder.orderCost) && paymentMethod == pizzaOrder.paymentMethod && Objects.equals(comment, pizzaOrder.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderedPizzas, orderCost, paymentMethod, comment);
    }
}
