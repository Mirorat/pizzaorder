package com.mirorat.model.pizza;


class Calzone extends AbstractPizza implements Pizza {
    public Calzone() {
        this.type = Type.CALZONE;
    }



    static class Builder extends AbstractPizza.Builder{
        Builder(Size size) {
            super(size);
        }

        Pizza build(){
            Calzone pizza = new Calzone();
            setValues(pizza);
            return pizza;
        }
    }
}
