package com.mirorat.model.pizza;

public interface PizzaFactory {

    Pizza createPizza(Type type, Size size);
}
