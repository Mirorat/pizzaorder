package com.mirorat.model.pizza;

class Funghi extends AbstractPizza implements Pizza {
    public Funghi() {
        this.type = Type.FUNGHI;
    }

    static class Builder extends AbstractPizza.Builder{
        Builder(Size size) {
            super(size);
        }
        @Override
        Pizza build() {
            Funghi pizza = new Funghi();
            setValues(pizza);
            return pizza;
        }
    }


}
