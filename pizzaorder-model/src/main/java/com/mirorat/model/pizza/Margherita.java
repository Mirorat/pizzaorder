package com.mirorat.model.pizza;

public class Margherita extends AbstractPizza implements Pizza{
    public Margherita() {
        this.type = Type.MARGHERITA;
    }

    static class Builder extends AbstractPizza.Builder{

        Builder(Size size) {
            super(size);
        }

        @Override
        Pizza build() {
            Margherita pizza = new Margherita();
            setValues(pizza);
            return pizza;
        }
    }
}
