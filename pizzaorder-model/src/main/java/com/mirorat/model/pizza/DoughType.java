package com.mirorat.model.pizza;

public enum DoughType {
    CLASSIC,
    NONE,
    CALZONE
}
