package com.mirorat.model.pizza;

import com.mirorat.model.ingredient.Cheese;
import com.mirorat.model.ingredient.Meat;
import com.mirorat.model.ingredient.Sauce;
import com.mirorat.model.ingredient.Vegetable;

import java.util.List;


public interface Pizza {
    Size getSize();
    List<Cheese> getCheese();
    List<Vegetable> getVegetables();
    List<Meat> getMeat();
    Sauce getSauce();
    Double getTotalCost();
    DoughType getDoughType();


}
