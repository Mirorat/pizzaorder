package com.mirorat.model.pizza;


import org.apache.commons.text.WordUtils;

public enum Size {
    SMALL(0.75),
    MEDIUM(1.00),
    LARGE(1.25);

    private double priceRatio;

    Size(double priceRatio) {
        this.priceRatio = priceRatio;
    }

    public double getPriceRatio() {
        return priceRatio;
    }

    public String getFormattedName() {
     return WordUtils.capitalizeFully(this.name());
    }
}
