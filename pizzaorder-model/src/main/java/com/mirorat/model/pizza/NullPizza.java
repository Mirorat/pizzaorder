package com.mirorat.model.pizza;

import com.mirorat.model.ingredient.Cheese;
import com.mirorat.model.ingredient.Meat;
import com.mirorat.model.ingredient.Sauce;
import com.mirorat.model.ingredient.Vegetable;

import java.util.ArrayList;
import java.util.List;

class NullPizza extends AbstractPizza implements Pizza{

    NullPizza() {
    }

    @Override
    public Size getSize() {
        return null;
    }

    @Override
    public DoughType getDoughType() {
        return DoughType.NONE;
    }

    @Override
    public List<Cheese> getCheese() {
        return new ArrayList<>();
    }

    @Override
    public List<Vegetable> getVegetables() {
        return new ArrayList<>();
    }

    @Override
    public List<Meat> getMeat() {
        return new ArrayList<>();
    }

    @Override
    public Sauce getSauce() {
        return null;
    }

    @Override
    public Double getTotalCost() {
        return 0.0d;
    }

}
