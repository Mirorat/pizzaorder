package com.mirorat.model.pizza;


class Capriciosa extends  AbstractPizza implements Pizza {
    Capriciosa() {
        this.type = Type.CAPRICIOSA;
    }
    static class Builder extends AbstractPizza.Builder{

        Builder(Size size) {
            super(size);
        }

        @Override
        Pizza build() {
            Capriciosa pizza = new Capriciosa();
            setValues(pizza);
            return pizza;
        }
    }
}

