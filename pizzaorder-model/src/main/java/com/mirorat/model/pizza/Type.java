package com.mirorat.model.pizza;


import org.apache.commons.text.WordUtils;

public enum Type {
    FUNGHI,
    MARGHERITA,
    CAPRICIOSA,
    CALZONE;

    public String getFormattedName() {
        return WordUtils.capitalizeFully(this.name());
    }


}
