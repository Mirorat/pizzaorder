package com.mirorat.model.pizza;

import com.mirorat.model.ingredient.Cheese;
import com.mirorat.model.ingredient.Meat;
import com.mirorat.model.ingredient.Sauce;
import com.mirorat.model.ingredient.Vegetable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


public class CurrentPizzaFactory implements PizzaFactory {


    String testxmlConstructor;
    String testXmlField;
    @Value("${currentpizzafactory.testatvalue}")
    String testAtValue;
    private final Logger currentPizzaFactoryLogger = LoggerFactory.getLogger(CurrentPizzaFactory.class);

    @Autowired
    public CurrentPizzaFactory(String testxml) {
        this.testxmlConstructor = testxml;
        System.out.println(this.testxmlConstructor);
        System.out.println("@Value: " + testAtValue);
    }


    public Pizza createPizza(Type type, Size size) {
        Pizza pizza;

        if(Type.FUNGHI.equals(type)){
            pizza = new Funghi.Builder(size)
                    .withDoughType(DoughType.CLASSIC)
                    .withSauce(Sauce.TOMATO)
                    .withCheese(Cheese.MOZZARELLA)
                    .withVegetable(Vegetable.MUSHROOMS)
                    .withMeat(Meat.HAM)
                    .build();
        } else if(Type.CAPRICIOSA.equals(type)){
            pizza = new Capriciosa.Builder(size)
                    .withDoughType(DoughType.CLASSIC)
                    .withSauce(Sauce.TOMATO)
                    .withCheese(Cheese.MOZZARELLA)
                    .withMeat(Meat.HAM)
                    .build();
        } else if(Type.MARGHERITA.equals(type)){
            pizza = new Margherita.Builder( size)
                    .withDoughType(DoughType.CLASSIC)
                    .withSauce(Sauce.TOMATO)
                    .withCheese(Cheese.MOZZARELLA)
                    .build();
        } else if(Type.CALZONE.equals(type)){
            pizza = new Calzone.Builder(size)
                    .withDoughType(DoughType.CALZONE)
                    .withSauce(Sauce.TOMATO)
                    .withCheese(Cheese.MOZZARELLA)
                    .withVegetable(Vegetable.PEPPER)
                    .withVegetable(Vegetable.MUSHROOMS)
                    .withMeat(Meat.HAM)
                    .build();
        } else {
            pizza = new NullPizza();
        }
        currentPizzaFactoryLogger.debug("Pizza returned by current pizza facotry: {}", pizza);
        return pizza;
    }

    public void setTestXmlField(String testXmlField) {
        this.testXmlField = testXmlField;
    }

    public String getTestAtValue() {
        return testAtValue;
    }
}
