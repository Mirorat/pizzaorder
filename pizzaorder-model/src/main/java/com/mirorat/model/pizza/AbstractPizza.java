package com.mirorat.model.pizza;

import com.mirorat.model.ingredient.Cheese;
import com.mirorat.model.ingredient.Meat;
import com.mirorat.model.ingredient.Sauce;
import com.mirorat.model.ingredient.Vegetable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


class AbstractPizza implements Pizza {
    Type type;
    private Size size;
    private Double totalCost;   
    private List<Cheese> cheese;
    private List<Vegetable> vegetables;
    private List<Meat> meat;
    private Sauce sauce;
    private DoughType doughType;



    static class Builder{
         Size size;
         List<Cheese> cheese = new ArrayList<>();
         List<Vegetable> vegetables = new ArrayList<>();
         List<Meat> meat = new ArrayList<>();
         Sauce sauce = Sauce.NONE;
         DoughType doughType;

        Builder (Size size){
            this.size = size;
        }

        Builder withVegetable(Vegetable vegetable){
            List<Vegetable> vegetablesList = this.vegetables;
            vegetablesList.add(vegetable);
            this.vegetables = vegetablesList;
            return this;
        }

        Builder withCheese(Cheese cheese){
            List<Cheese> cheeseList = this.cheese;
            cheeseList.add(cheese);
            this.cheese = cheeseList;
            return this;
        }

        Builder withMeat(Meat meat){
            List<Meat> meatList = this.meat;
            meatList.add(meat);
            this.meat = meatList;
            return this;
        }

        Builder withSauce(Sauce sauce){
            this.sauce = sauce;
            return this;
        }

        Builder withDoughType(DoughType doughType){
            this.doughType = doughType;
            return this;
        }



        Pizza build(){
            AbstractPizza pizza = new AbstractPizza();
            setValues(pizza);
            return pizza;
        }

        void setValues(AbstractPizza pizza){
            pizza.cheese = this.cheese;
            pizza.doughType = this.doughType;
            pizza.meat = this.meat;
            pizza.sauce = this.sauce;
            pizza.size = this.size;
            pizza.vegetables = this.vegetables;
            pizza.totalCost = calculateCost();
        }

        Double calculateCost(){
            double cost = cheese
                    .stream()
                    .mapToDouble(i -> i.getBaseCost() * size.getPriceRatio())
                    .sum();
            cost += vegetables
                    .stream()
                    .mapToDouble(i -> i.getBaseCost() * size.getPriceRatio())
                    .sum();
            cost += meat
                    .stream()
                    .mapToDouble(i -> i.getBaseCost() * size.getPriceRatio())
                    .sum();
            cost += sauce.getBaseCost() * size.getPriceRatio();

            return cost;
        }

    }

    public Size getSize() {
        return size;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public List<Cheese> getCheese() {
        return cheese;
    }

    public List<Vegetable> getVegetables() {
        return vegetables;
    }

    public List<Meat> getMeat() {
        return meat;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public DoughType getDoughType() {
        return doughType;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractPizza that = (AbstractPizza) o;
        return size == that.size &&
                Objects.equals(totalCost, that.totalCost) &&
                Objects.equals(cheese, that.cheese) &&
                Objects.equals(vegetables, that.vegetables) &&
                Objects.equals(meat, that.meat) &&
                sauce == that.sauce &&
                doughType == that.doughType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, totalCost, cheese, vegetables, meat, sauce, doughType);
    }

    @Override
    public String toString() {
        return size.getFormattedName() + " " + type.getFormattedName();
    }
}
