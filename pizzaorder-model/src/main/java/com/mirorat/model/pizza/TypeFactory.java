package com.mirorat.model.pizza;

import java.util.Optional;

public class TypeFactory {
    public AbstractPizza getPizzaType(String name){
        AbstractPizza pizza;
        Optional<String> checkName = Optional.ofNullable(name);
        name = checkName.orElse("Value not passed");
        name = name.toUpperCase();

        if(Type.FUNGHI.name().equals (name)){
            pizza = new Funghi();
        } else if(Type.CAPRICIOSA.name().equals(name)){
            pizza =  new Capriciosa();
        } else if(Type.MARGHERITA.name().equals(name)){
            pizza = new Margherita();
        } else if(Type.CALZONE.name().equals(name)){
            pizza =  new Calzone();
        } else {
            pizza = new NullPizza();
        }

        return pizza;
    }
}
