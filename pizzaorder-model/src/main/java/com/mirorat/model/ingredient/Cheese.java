package com.mirorat.model.ingredient;

public enum Cheese {
    MOZZARELLA(3),
    PARMESAN(5);

    private double baseCost;

    Cheese(double baseCost) {
        this.baseCost = baseCost;
    }

    public double getBaseCost() {
        return baseCost;
    }
}
