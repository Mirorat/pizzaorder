package com.mirorat.model.ingredient;

public enum Sauce {
    TOMATO(3),
    NONE(0);

    private double baseCost;

    Sauce(double baseCost) {
        this.baseCost = baseCost;
    }

    public double getBaseCost() {
        return baseCost;
    }
}
