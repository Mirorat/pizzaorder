package com.mirorat.model.ingredient;


import org.springframework.stereotype.Component;

@Component
public class Milk {
    private String milkType;

    public Milk() {
        this.milkType = "Cow milk";
    }

    public String getMilkType() {
        return milkType;
    }

    public void setMilkType(String milkType) {
        this.milkType = milkType;
    }
}
