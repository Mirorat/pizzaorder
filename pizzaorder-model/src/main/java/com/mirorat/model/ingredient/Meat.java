package com.mirorat.model.ingredient;

public enum Meat {
    HAM(3),
    CHICKEN(4);

    private double baseCost;

    Meat(double baseCost) {
        this.baseCost = baseCost;
    }

    public double getBaseCost() {
        return baseCost;
    }
}
