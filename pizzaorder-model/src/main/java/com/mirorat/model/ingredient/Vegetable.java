package com.mirorat.model.ingredient;

public enum Vegetable {
    MUSHROOMS(3),
    PEPPER(2);

    private double baseCost;

    Vegetable(double baseCost) {
        this.baseCost = baseCost;
    }

    public double getBaseCost() {
        return baseCost;
    }
}
