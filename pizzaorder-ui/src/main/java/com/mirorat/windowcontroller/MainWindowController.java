package com.mirorat.windowcontroller;

import com.mirorat.model.PaymentMethod;
import controller.Controller;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class MainWindowController implements Initializable {
    private final Logger mainWindowControllerLogger = LoggerFactory.getLogger(MainWindowController.class);

    @Autowired
    private Controller controller;
    @FXML
    private TextField pizzaNameField;
    @FXML
    private ComboBox sizes;
    @FXML
    public Button addToOrder;
    @FXML
    private ListView currentOrder;
    @FXML
    public Button removeFromOrder;
    @FXML
    private ListView totalCost;
    @FXML
    private ComboBox paymentMethods;
    @FXML
    private TextArea comment;
    @FXML
    public Button confirmOrder;
    @FXML
    private ListView allOrders;


    private ObservableList<String> sizesList;
    private ObservableList<String> paymentMethodsList;
    private ObservableList<String> orders;
    private ObservableList<String> currentOrderList;


    public MainWindowController(){
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sizesList = FXCollections.observableArrayList(controller.getAllSizes());
        paymentMethodsList = FXCollections.observableList(controller.getAllPaymentMethods());
        orders = FXCollections.observableArrayList(controller.getOrders());
        mainWindowControllerLogger.debug("MainWindowController initialized, com.mirorat.controller.getOreders() returned {}", controller.getOrders());


        sizes.setValue(sizesList.get(0));
        sizes.setItems(sizesList);

        paymentMethods.setItems(paymentMethodsList);
        paymentMethods.setValue(paymentMethodsList.get(0));

        allOrders.setItems(orders);

        currentOrderList = FXCollections.observableArrayList();
    }

    @FXML
    public void addToCurrentOrder() {
        if (isInputCorrect()) {
            controller.addPizzaToCurrentOrder(controller.createPizza(pizzaNameField.getText(), sizes.getValue().toString()));
            totalCost.setItems(FXCollections.observableArrayList(controller.getCurrentOrderCost().toString()));
//            int orderSize = com.mirorat.controller.currentOrder.getOrderedPizzas().size();
//            currentOrderList.add(com.mirorat.controller.currentOrder.getOrderedPizzas().get(orderSize - 1).toString());
            currentOrder.setItems(currentOrderList);
            pizzaNameField.setPromptText("Please type one of our pizza: Funghi, Capriciosa, Calzone, Margherita");
        } else{
            pizzaNameField.setText("");
            pizzaNameField.setPromptText("Invalid pizza type selected - please type one of our pizza: Funghi, Capriciosa, Calzone, Margherita");
        }
    }

    @FXML
    public void removeFromOrder(){
        int pizzaToRemoveIndex = currentOrder.getSelectionModel().getSelectedIndex();
//        Pizza pizzaToRemove = com.mirorat.controller.getCurrentOrderPizzas().get(pizzaToRemoveIndex);
//        com.mirorat.controller.removePizzaFromCurrentOrder(pizzaToRemove);
        currentOrderList.remove(pizzaToRemoveIndex);
        currentOrder.setItems(currentOrderList);
        totalCost.setItems(FXCollections.observableArrayList(controller.getCurrentOrderCost().toString()));
    }

    @FXML
    public void confirmOrder(){
        PaymentMethod paymentMethod = PaymentMethod.valueOf(paymentMethods.getValue().toString().toUpperCase());
        controller.setCurrentOrderPaymentMethod(paymentMethod);
        controller.setCurrentOrderComment(comment.getText());
        controller.confirmCurrentOrder();
        orders = FXCollections.observableArrayList(controller.getOrders());
        currentOrder.getItems().clear();
        allOrders.setItems(orders);
        mainWindowControllerLogger.debug("allOrders items set to: {}", allOrders.getItems());
        mainWindowControllerLogger.debug("order confirmed");
    }

    @FXML
    public void exit(){
        Platform.exit();
    }

    public boolean isInputCorrect(){
        return pizzaNameField.getText().toUpperCase().matches("CAPRICIOSA|FUNGHI|CALZONE|MARGHERITA");
    }
}
