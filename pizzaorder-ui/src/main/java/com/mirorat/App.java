package com.mirorat;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Import({ ApplicationConfiguration.class }) //loads another JavaConfig
@ImportResource("classpath:config.xml")
@SpringBootApplication()
@EnableJpaRepositories("repository")
public class App extends Application {


    private ConfigurableApplicationContext springContext;
    private Parent rootNode;
    private FXMLLoader loader;
    private final Logger LOG = LoggerFactory.getLogger("src.main.java.com.selflearning.com.mirorat.App");

    @Override
    public void start(Stage primaryStage) throws Exception{

        loader.setLocation(this.getClass().getResource("/fxmls/MainWindow.fxml"));
        LOG.info("com.mirorat.App info log TEST");

        rootNode = loader.load();
        LOG.debug("com.mirorat.App Debug log TEST");
        LOG.debug("root node loaded: {}", rootNode);
        Scene scene = new Scene(rootNode);

        LOG.warn("com.mirorat.App Warn log TEST");
        primaryStage.setScene(scene);
        primaryStage.setTitle("Mirek's PIZZA!");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(App.class, args);
    }

    @Override
    public void init() {
        springContext = SpringApplication.run(App.class);
        loader = new FXMLLoader();
        loader.setControllerFactory(springContext::getBean);
    }

    @Override
    public void stop() {
        springContext.close();
    }
}
