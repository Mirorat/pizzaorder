package com.mirorat;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.mirorat", "com.mirorat.windowcontroller", "repository", "service", "controller", })
public class ApplicationConfiguration {


}
