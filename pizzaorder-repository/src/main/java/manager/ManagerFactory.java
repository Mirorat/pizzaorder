package manager;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@Component
public class ManagerFactory {
    javax.persistence.EntityManagerFactory entityManagerFactory;


    public ManagerFactory() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("com.mirorat.pizzaordercatalog");
        SessionFactory factory =  entityManagerFactory.unwrap(SessionFactory.class);
    }


    public SessionFactory getSessionFactory(){
        SessionFactory factory = entityManagerFactory.unwrap(SessionFactory.class);
        return factory;
    }

    public EntityManager getEntityManager() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager;
    }
}
