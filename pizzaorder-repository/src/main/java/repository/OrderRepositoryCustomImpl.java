package repository;

import com.mirorat.model.PizzaOrder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


@Repository
public class OrderRepositoryCustomImpl implements OrderRepositoryCustom{

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("com.mirorat.pizzaordercatalog");
    SessionFactory factory =  entityManagerFactory.unwrap(SessionFactory.class);
    EntityManager entityManager = entityManagerFactory.createEntityManager();


    @Override
    public List<PizzaOrder> findOrdersWithCostGreaterThan(Double cost) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PizzaOrder> criteriaQuery = criteriaBuilder.createQuery(PizzaOrder.class);
        Root<PizzaOrder> root = criteriaQuery.from(PizzaOrder.class);
        criteriaQuery.select(root).where(criteriaBuilder.ge(root.get("orderCost"), cost));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<PizzaOrder> findOrdersWithCostGreaterThanHQL(Double cost) {
        String HQL = "FROM com.mirorat.model.PizzaOrder WHERE orderCost > :cost";
        Session session = factory.openSession();
        Query query = session.createQuery(HQL).setParameter("cost", cost);
        List<PizzaOrder> results = query.getResultList();

        return results;
    }

    public OrderRepositoryCustomImpl() {
    }
}
