package repository;

import com.mirorat.model.PizzaOrder;

import java.util.List;


public interface OrderRepositoryCustom {

    List<PizzaOrder> findOrdersWithCostGreaterThan(Double cost);

    List<PizzaOrder> findOrdersWithCostGreaterThanHQL(Double cost);
}
