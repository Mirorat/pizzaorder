package repository;

import com.mirorat.model.PizzaOrder;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<PizzaOrder, Integer> {

    List<PizzaOrder> findPizzaOrderByOrderCostGreaterThan(Double cost);

    @Query(value = "SELECT p FROM com.mirorat.model.PizzaOrder p where p.orderCost > ?1")
    List<PizzaOrder> findOrdersWithCostGreaterThanCustom(Double cost);


}
